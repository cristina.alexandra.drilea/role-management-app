#include "HandleRole.hpp"

using namespace rapidjson;

HandleRole::HandleRole(){}

HandleRole::HandleRole(const char* filepath): inputFile(filepath)
{
	inputFile.open({ R"(filepath)"});
	if (!inputFile.is_open())
	{
		std::cerr << "Could not open file for reading!\n";
	}
	std::string str((std::istreambuf_iterator<char>(inputFile)),
		std::istreambuf_iterator<char>());
	doc.Parse(str.c_str());
	if (doc.HasParseError())
	{
		std::cout << "Error  : " << doc.GetParseError() << '\n'
			<< "Offset : " << doc.GetErrorOffset() << '\n';
	}
}

const std::string& HandleRole::getRoleManagementPath()
{
	return roleManagementPath;
}

const std::string& HandleRole::getParentContainerPath()
{
	return parentContainerPath;
}

void HandleRole::listAllRolesToConsole() {
	const Value& roles = doc["roles"];
	StringBuffer sb;
	PrettyWriter<StringBuffer> writer(sb);
	doc.Accept(writer);    
	puts(sb.GetString());
}

void HandleRole::listParentContainerToConsole() {
	const Value& roles = doc["container"];
	StringBuffer sb;
	PrettyWriter<StringBuffer> writer(sb);
	doc.Accept(writer);
	puts(sb.GetString());
}

void HandleRole::printAttributeValueRoleManagement() {
	const Value& roles = doc["roles"];

	std::cout << "______Printing attribute values________\n ";
	std::cout << "The business codes are: \n" << roles[0]["businessCode"].GetString() << std::endl;
	std::cout << roles[1]["businessCode"].GetString() << std::endl;
	std::cout << roles[2]["businessCode"].GetString() << std::endl;
}


void HandleRole::printAttributeValueParentContainer() {
	const Value& container = doc["container"];

	std::cout << "______Printing attribute values________\n ";
	std::cout << "The names for each object are: \n" << container[0]["name"].GetString() << std::endl;
	std::cout << container[1]["name"].GetString() << std::endl;
	std::cout << container[2]["name"].GetString() << std::endl;
}

void HandleRole::createNewRole()
{
	rm.setId("114");
	rm.setName("scrum master");
	rm.setStartDate("2013-06-05");
	rm.setEndDate("2019-06-05");
	rm.setBusinessCode("011d");
	rm.setDescription("Perfectjob");
}

void HandleRole::displayNewRole() {
	createNewRole();
	std::cout << "Id: " << rm.getId() << std::endl;
	std::cout << "Name: " << rm.getName().c_str() << std::endl;
	std::cout << "StartDate: " << rm.getStartDate().c_str() << std::endl;
	std::cout << "EndDate: " << rm.getEndDate().c_str() << std::endl;
	std::cout << "BusinessCode: " << rm.getBusinessCode().c_str() << std::endl;
	std::cout << "Description:" << rm.getDescription().c_str() << std::endl;
}

void HandleRole::addRole()
{
	newRoles.emplace_back(rm.getId(), rm.getName(), rm.getStartDate(), rm.getEndDate(), rm.getBusinessCode(), rm.getDescription());
}

void HandleRole::insertNewRole()
{
	bool exists = false;
	createNewRole();
	StringBuffer buffer{};
	const std::string jsonStr{ buffer.GetString() };
	std::cout << jsonStr.c_str() << '\n';

	Value& roles = doc["roles"];
	Value idv, nmv, stDtv, edDtv, prCnv, bssv;
	Document::AllocatorType& allocator = doc.GetAllocator();

	std::string id = rm.getId();
	std::string nm = rm.getName();
	std::string stDt = rm.getStartDate();
	std::string edDt = rm.getEndDate();
	std::string prCn = rm.getId();
	std::string bss = rm.getBusinessCode();
	
	idv.SetString(id.c_str(), allocator);
	nmv.SetString(nm.c_str(), allocator);
	stDtv.SetString(stDt.c_str(), allocator);
	edDtv.SetString(edDt.c_str(), allocator);
	prCnv.SetString(prCn.c_str(), allocator);
	bssv.SetString(bss.c_str(), allocator);
	for (SizeType i = 0; i < roles.Size(); ++i) {
		if (roles[i]["id"].GetString() == id) {
			exists = true;
			break;
		}
	}

	Value obj(kObjectType);
	if (exists == false) {
		obj.AddMember("id", idv, allocator);
		obj.AddMember("name", nmv, allocator);
		obj.AddMember("startDate", stDtv, allocator);
		obj.AddMember("endDate", edDtv, allocator);
		obj.AddMember("parentContainerID", prCnv, allocator);
		obj.AddMember("businessCode", bssv, allocator);
		obj.AddMember("riskType", "medium", allocator);
		obj.AddMember("roleType", "technical", allocator);
		roles.PushBack(obj, allocator);
	}
}

void HandleRole::replaceToJson() {
	StringBuffer buffer{};
	const std::string jsonStr{ buffer.GetString() };
	std::cout << jsonStr.c_str() << '\n';

	Value& roles = doc["roles"];
	Document::AllocatorType& allocator = doc.GetAllocator();

	roles[1]["name"] = "changed value";
}

void HandleRole::eraseObject()
{

	StringBuffer buffer{};
	const std::string jsonStr{ buffer.GetString() };
	std::cout << jsonStr.c_str() << '\n';

	Value& roles = doc["roles"];

	std::cout << "\n Erasing object, check your JSON \n";
	roles.Erase(roles.Begin());
}

void HandleRole::combineTwoJsonFiles()
{
	// open RoleManagement JSON file
	inputFileRoles.open({ rolePath });
	if (!inputFileRoles.is_open())
	{
		std::cerr << "Could not open file for reading!\n";
	}
	std::string strRoles((std::istreambuf_iterator<char>(inputFileRoles)),
		std::istreambuf_iterator<char>());
	docRoles.Parse(strRoles.c_str());
	if (docRoles.HasParseError())
	{
		std::cout << "Error  : " << docRoles.GetParseError() << '\n'
			<< "Offset : " << docRoles.GetErrorOffset() << '\n';
	}

	// open ParentContainer JSON file
	inputFileContainer.open({ parentContainerPath });
	if (!inputFileContainer.is_open())
	{
		std::cerr << "Could not open file for reading!\n";
	}
	std::string strContainer((std::istreambuf_iterator<char>(inputFileContainer)),
		std::istreambuf_iterator<char>());
	docContainer.Parse(strContainer.c_str());
	if (docContainer.HasParseError())
	{
		std::cout << "Error  : " << docContainer.GetParseError() << '\n'
			<< "Offset : " << docContainer.GetErrorOffset() << '\n';
	}

	const Value& roles = docRoles["roles"];
	const Value& container = docContainer["container"];
	for (SizeType i = 0; i < roles.Size(); ++i) {
		for (SizeType j = 0; j < container.Size(); ++j) {
			std::string roleName = roles[i]["name"].GetString();
			std::string roleContainerID = roles[i]["parentContainerID"].GetString();
 			std::string parentContainerID = container[j]["id"].GetString();
			std::string parentContainerValue = container[j]["name"].GetString();
			if (roleContainerID == parentContainerID) {
				std::cout << "The parrent container for " << roleName << " is " << parentContainerValue << std::endl;
				break;
			}
		}
	}
}

HandleRole::~HandleRole()
{
	std::ofstream ofs{ R"(C:\Files\Atos\RoleManagementSimulationApp\RoleManagement.json)" };
	if (!ofs.is_open())
	{
		std::cerr << "Could not open file for writing!\n";
	}

	OStreamWrapper osw{ ofs };
	PrettyWriter<OStreamWrapper> writer2{ osw };
	doc.Accept(writer2); // Accept() traverses the DOM and generates Handler events.
}
