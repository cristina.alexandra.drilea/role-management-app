#include "RoleManagement.hpp"

RoleManagement::RoleManagement()
	//int i, std::string n, std::string st, std::string end, std::string bss, std::string des):
	//m_id(i), m_name(n), m_startDate(st), m_endDate(end), m_businessCode(bss), m_description(des)
{
}

RoleManagement::RoleManagement(std::string i, std::string n, std::string st, std::string end, std::string bss, std::string des):
	m_id(i), m_name(n), m_startDate(st), m_endDate(end), m_businessCode(bss), m_description(des)
{
}

const std::string& RoleManagement::getId()
{
	return m_id;
}

const std::string& RoleManagement::getName()
{
	return m_name;
}

const std::string& RoleManagement::getStartDate()
{
	return m_startDate;
}

const std::string& RoleManagement::getEndDate()
{
	return m_endDate;
}

const std::string& RoleManagement::getBusinessCode()
{
	return m_businessCode;
}

const std::string& RoleManagement::getDescription()
{
	return m_description;
}

void RoleManagement::setId(std::string id)
{
	m_id = id;
}

void RoleManagement::setName(std::string  name)
{
	m_name = name;
}

void RoleManagement::setStartDate(std::string startDate)
{
	m_startDate = startDate;
}

void RoleManagement::setEndDate(std::string endDate)
{
	m_endDate = endDate;
}

void RoleManagement::setBusinessCode(std::string businessCode)
{
	m_businessCode = businessCode;
}

void RoleManagement::setDescription(std::string description)
{
	m_description = description;

}


RoleManagement::~RoleManagement()
{
}
