#pragma once 
#include<iostream>
class ParentContainer
{
	int m_id;
	std::string m_name;
	std::string m_description;
public:
	const int getId();
	const std::string& getName();
	const std::string& getDescription();
	ParentContainer();
	~ParentContainer();
};

