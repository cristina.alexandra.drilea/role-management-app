#include <iostream>
#include <fstream>
#include <assert.h>
#include <vector>
#include <string>
#include "rapidjson/document.h"

#include "RoleManagement.hpp"

static constexpr const char* const rolePath = R"(C:\Files\RoleManagementSimulationApp\RoleManagement.json)";
static constexpr const char* const parentPath = R"(C:\Files\RoleManagementSimulationApp\ParentContainer.json)"; 
class HandleRole
{
	RoleManagement rm;
	std::vector<RoleManagement> newRoles;
	std::ifstream inputFile, inputFileRoles, inputFileContainer;
	rapidjson::Document doc, docRoles, docContainer;
	std::string roleManagementPath { R"(C:\Files\RoleManagementSimulationApp\RoleManagement.json)" };
	std::string parentContainerPath { R"(C:\Files\RoleManagementSimulationApp\ParentContainer.json)" };

public:
	void listAllRolesToConsole();
	void listParentContainerToConsole();
	void printAttributeValueRoleManagement();
	void printAttributeValueParentContainer();
	void createNewRole();
	void displayNewRole();
	void addRole();
	void insertNewRole();
	void replaceToJson();
	void eraseObject();
	void combineTwoJsonFiles();
	const std::string& getRoleManagementPath();
	const std::string& getParentContainerPath();
	HandleRole(const char* filepath);
	HandleRole();
	~HandleRole();
};

