#pragma once
#include<iostream>
#include "ParentContainer.hpp"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/prettywriter.h"


enum class RiskType {
	Low,
	Medium,
	High,
	Critical
};

class RoleManagement
{
	std::string m_id;
	std::string m_name;
	std::string m_startDate;
	std::string m_endDate;
	std::string m_businessCode;
	std::string m_description;
	/*ParentContainer parentContainerID;
	RiskType m_riskType;*/

public:
	RoleManagement();
	RoleManagement(std::string i, std::string n, std::string st, std::string end, std::string bss, std::string des);
		//int i, std::string n, std::string st, std::string end, std::string bss, std::string des);
	const std::string& getId();
	const std::string& getName();
	const std::string& getStartDate();
	const std::string& getEndDate();
	const std::string& getBusinessCode();
	const std::string& getDescription();
	void setId(std::string id);
	void setName(std::string name);
	void setStartDate(std::string startDate);
	void setEndDate(std::string endDate);
	void setBusinessCode(std::string businessCode);
	void setDescription(std::string description);
	~RoleManagement();
};

