#include "ParentContainer.hpp"

const int ParentContainer::getId()
{
	return m_id;
}

const std::string& ParentContainer::getName()
{
	return m_name;
}

const std::string& ParentContainer::getDescription()
{
	return m_description;
}

ParentContainer::ParentContainer()
{
	std::cout << "Constructor for ParentContainer class has been callled" << std::endl;
}


ParentContainer::~ParentContainer()
{
	std::cout << "Destructor for ParentContainer class has been callled" << std::endl;
}
