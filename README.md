This application is written in C++. It handles the roles that exist in an organization. 
Basically it parse 2 JSON files which are populated with informations about the roles in the organization and about their parent containers.
The 2 JSON files must be at the path specified in the HandleRoles.hpp (or change the absolute path of the 2 variables). Both of the files can be found in this repository. 
Using this application many opperations can be performed as: list the roles, list the values of the atrributes, add an object, delete an object and so on, combine the 2 JSON files in order to print the role and the parent container which belongs to that role and so on.

NOTE: RAPIDJSON was used as parser and generator for C++.
